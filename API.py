import requests

url = "https://api.darksky.net/forecast/f76287f51846b0fd446527ec5e88c1f1/-34.6083,-58.3712"
response = requests.get(url)

print(response)

if response.status_code == 200:
    content = response.content

    file = open("darksky.html", "wb")
    file.write(content)
    file.close()